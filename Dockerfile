FROM node:14.18-alpine
WORKDIR /app
COPY package.json ./
RUN npm install
COPY . .
ENV CI=true
RUN npm test
CMD ["npm", "start"]