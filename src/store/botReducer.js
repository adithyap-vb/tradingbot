const botData = require("../data.js");

const botState = {
  botData,
  cart: [],
};

const botReducer = (state = botState, action) => {
  switch (action.type) {
    case "addToCart":
      return {
        botData,

        cart: [
          ...state.cart,
          {
            id: action.payload.id,
            bot: action.payload.bot,
            description: action.payload.description,
            indexValue: action.payload.indexValue,
            cagr: action.payload.cagr,
            qty: 1,
          },
        ],
      };

    case "removeFromCart":
      return {
        botData,
        cart: state.cart.filter((element) => element.id != action.payload.id),
      };

    default:
      return {
        botData,
        cart: [],
      };
  }
};

export default botReducer;
