import { createStore } from "redux";
import botReducer from "./botReducer";

const store = createStore(botReducer);

export default store;
