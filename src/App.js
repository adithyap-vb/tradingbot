import "./App.css";

import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";
import BotList from "./components/BotList";
import BotDetails from "./components/BotDetails";
import Cart from "./components/Cart";
import { useSelector } from "react-redux";
import AppBar from "./components/AppBar";
import { Provider } from "react-redux";
import store from "./store/store.js";
function App() {
  //const res = useSelector((state) => state);
  return (
    <Provider store={store}>
      <Router>
        <AppBar />
        <Switch>
          <Redirect exact from="/" to="/bots" />
          <Route exact path="/bots" component={BotList} />
          <Route exact path="/bots-details/:botId" component={BotDetails} />
          <Route exact path="/cart" component={Cart} />
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
