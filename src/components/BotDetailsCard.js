import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import "./BotCard.css";
function BotDetailsCard(props) {
  const dispatch = useDispatch();
  let len = 0;
  const cartItems = useSelector((state) => state.cart);
  const [cartLen, setCartLen] = useState(getCartLen());
  function getCartLen() {
    console.log("getCartLen called");
    let len = 0;
    cartItems.forEach((element) => {
      if (props.id === element.id) {
        len += 1;
      }
    });
    return len;
  }
  useEffect(() => {
    let len = 0;
    console.log(cartItems);
    cartItems.forEach((element) => {
      console.log(typeof element.id);
      console.log(typeof props.id);
      if (props.id == element.id) {
        len += 1;
      }
    });
    setCartLen(len);
  }, []);
  useEffect(() => {
    let len = 0;
    cartItems.forEach((element) => {
      if (props.id == element.id) {
        len += element.qty;
      }
    });
    setCartLen(len);
  }, [cartItems]);

  const addToCart = () => {
    dispatch({
      type: "addToCart",
      payload: {
        id: props.id,
        bot: props.title,
        description: props.desc,
        indexValue: props.indexValue,
        cagr: props.cagr,
      },
    });
    setCartLen(cartLen + 1);
  };
  const removeFromCart = () => {
    dispatch({
      type: "removeFromCart",
      payload: {
        id: props.id,
      },
    });
  };

  return (
    <div className="card">
      <div className="botDetails">
        <h2 className="cardTitle">
          <span className="titleStyle">Bot Name: </span>
          {props.title}
        </h2>
        <h2 className="cardDesc">
          <span className="descStyle">Bot Description: </span>

          {props.desc}
        </h2>
        <h2>
          <span className="descStyle">Bot Index Value: </span>

          {props.indexValue}
        </h2>
        <h2>
          <span className="descStyle">Bot CAGR: </span>

          {props.cagr}
        </h2>
      </div>
      <div className="actionsContainer">
        {props.id == 1 && (
          <img src="https://img.icons8.com/color-glass/50/000000/bot.png" />
        )}
        {props.id == 2 && (
          <img src="https://img.icons8.com/offices/40/000000/bot.png" />
        )}
        {props.id == 3 && (
          <img src="https://img.icons8.com/color/48/000000/bot.png" />
        )}
        <button onClick={addToCart} className="addToCartButton">
          Add To Cart
        </button>
        <h3>{cartLen}</h3>
        <button onClick={removeFromCart} className="addToCartButton">
          Remove From Cart
        </button>
      </div>
    </div>
  );
}
export default BotDetailsCard;
