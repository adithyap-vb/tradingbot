import React, { useState, useEffect } from "react";

import { Provider, useSelector } from "react-redux";
import { useHistory } from "react-router";
import store from "../store/store.js";

import "./AppBar.css";
function AppBar() {
  const cartItems = useSelector((state) => state.cart);
  const [cartLen, setCartLen] = useState(0);
  useEffect(() => {
    let len = 0;
    cartItems.forEach((element) => {
      len += element.qty;
    });
    setCartLen(len);
  }, [cartItems]);

  const history = useHistory();

  return (
    <Provider store={store}>
      <div className="container">
        <div className="hero">
          <h1 className="title">Trading Bots</h1>
          <img src="https://img.icons8.com/ios-filled/50/000000/bot.png" />
        </div>
        <div className="actions">
          <img src="https://img.icons8.com/ios-filled/50/000000/shopping-cart.png" />
          <h2>{cartLen}</h2>
          <h3>Items in Cart</h3>
        </div>
      </div>
    </Provider>
  );
}

export default AppBar;
