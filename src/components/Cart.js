//Cart Page is not implemented
import React from "react";
import { useSelector } from "react-redux";

function Cart() {
  const cartItems = useSelector((state) => state.cart);
  return (
    <div>
      <h2>Inside Cart</h2>
      {cartItems.map((item) => {
        return (
          <div key={item.id}>
            <h3>{item.bot}</h3>
          </div>
        );
      })}
    </div>
  );
}

export default Cart;
