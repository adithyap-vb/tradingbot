import React from "react";
import { useDispatch, useSelector } from "react-redux";
import BotCard from "./BotCard";

import "./BotList.css";
function BotList() {
  const dispatch = useDispatch();
  const addBotToCart = () => {
    dispatch({
      type: "addToCart",
      payload: {
        id: 15,
      },
    });
  };
  const botData = useSelector((state) => state.botData);
  return (
    <div className="listContainer">
      <h2>Bots List</h2>
      {console.log(botData.length)}
      {botData.map((item) => {
        return (
          <BotCard
            id={item.id}
            key={item.id}
            title={item.bot}
            desc={item.description}
            indexValue={item.indexValue}
            cagr={item.cagr}
          />
        );
      })}
      <a href="https://icons8.com/icon/HRqoDlVZAD8t/bot">Bot icon by Icons8</a>
      <a href="https://icons8.com/icon/59023/bot">Bot icon by Icons8</a>
      <a href="https://icons8.com/icon/d4H6cwfT2f-K/bot">Bot icon by Icons8</a>
      <a href="https://icons8.com/icon/9720/shopping-cart">
        Shopping Cart icon by Icons8
      </a>
    </div>
  );
}
export default BotList;
