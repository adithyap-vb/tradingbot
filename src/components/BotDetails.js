import React, { useState } from "react";
import BotDetailsCard from "./BotDetailsCard";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import "./BotDetails.css";
function BotDetails() {
  const { botId } = useParams();
  let bot = "";
  let botDesc = "";
  let botIndexValue = 0;
  let botCagr = 0;
  let qty = 0;
  const algoDesc = useSelector((state) => {
    state.botData.forEach((element) => {
      if (element.id == botId) {
        bot = element.bot;
        botDesc = element.description;
        botIndexValue = element.indexValue;
        botCagr = element.cagr;
        qty = element.qty;
      }
    });
  });
  return (
    <div className="detailsContainer">
      <h2>Bot details</h2>
      <BotDetailsCard
        id={botId}
        title={bot}
        desc={botDesc}
        cagr={botCagr}
        indexValue={botIndexValue}
        qty={qty}
      />
      <span className="homeLink">
        <Link to="/bots">Back To Home</Link>
      </span>
    </div>
  );
}

export default BotDetails;
