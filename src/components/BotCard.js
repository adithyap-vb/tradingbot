import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import "./BotCard.css";
function BotCard(props) {
  const dispatch = useDispatch();
  const history = useHistory();
  const cartItems = useSelector((state) => state.cart);
  const [cartLen, setCartLen] = useState(0);
  useEffect(() => {
    let len = 0;
    cartItems.forEach((element) => {
      if (props.id == element.id) {
        len += element.qty;
      }
    });
    setCartLen(len);
  }, [cartItems]);

  const addToCart = () => {
    dispatch({
      type: "addToCart",
      payload: {
        id: props.id,
        bot: props.title,
        description: props.desc,
        indexValue: props.indexValue,
        cagr: props.cagr,
      },
    });
    setCartLen(cartLen + 1);
  };

  const removeFromCart = () => {
    dispatch({
      type: "removeFromCart",
      payload: {
        id: props.id,
      },
    });
  };
  const showAlgo = () => {
    history.push("/bots-details/" + props.id);
  };

  return (
    <div className="card">
      <div className="botDetails">
        <h2 className="cardTitle">
          <span className="titleStyle">Bot Name: </span>
          {props.title}
        </h2>
        <h2>
          <span className="descStyle">Bot Index Value: </span>

          {props.indexValue}
        </h2>
        <h2>
          <span className="descStyle">Bot CAGR: </span>

          {`${props.cagr}%`}
        </h2>
      </div>
      <div className="actionsContainer">
        {props.id == 1 && (
          <img src="https://img.icons8.com/color-glass/50/000000/bot.png" />
        )}
        {props.id == 2 && (
          <img src="https://img.icons8.com/offices/40/000000/bot.png" />
        )}
        {props.id == 3 && (
          <img src="https://img.icons8.com/color/48/000000/bot.png" />
        )}
        <button onClick={showAlgo} className="showAlgoButton">
          View Algo{" "}
        </button>
        <button onClick={addToCart} className="addToCartButton">
          Add To Cart
        </button>
        <h3>{`${cartLen} items in cart`}</h3>
        <button onClick={removeFromCart} className="addToCartButton">
          Remove From Cart
        </button>
      </div>
    </div>
  );
}
export default BotCard;
