const data = {
  bots: [
    {
      id: 1,
      bot: "Hot Bot",
      description:
        "Hot bot is low volatility portfolio of growth stocks selected based on our proprietary metrics",
      indexValue: 289.34,
      cagr: 34,
      qty: 0,
    },
    {
      id: 2,
      bot: "Cool Bot",
      description:
        "Cool bot is low volatility portfolio of growth stocks selected based on our proprietary metrics",
      indexValue: 439.34,
      cagr: 28,
      qty: 0,
    },
    {
      id: 3,
      bot: "Options Bot",
      description:
        "Options bot is low volatility portfolio of growth stocks selected based on our proprietary metrics",
      indexValue: 139.34,
      cagr: 42,
      qty: 0,
    },
  ],
};

module.exports = data.bots;
